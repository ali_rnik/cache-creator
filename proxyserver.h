#ifndef PROXYSERVER_H
#define PROXYSERVER_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/select.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <limits.h>

#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <fstream>

#include "myhash.h"
#include "xattribute.h"

#define CRLF "\r\n"
#define CHUNK_END "0\r\n\r\n"
#define READ_MAX 5000
#define DATABASE_ADDRESS "/home/strong/DB/"

struct Userdata;

int server_startup(int port);
void ferror(int line, const char* func);
void* handle_client(void* connected_sock);
size_t curl_write(void* ptr, size_t sz, size_t nmemb, Userdata* ud);

/*
  Store data which needed to pass to curl_write function.
*/
struct Userdata
{
  int connected_sock;
  bool is_chunk, passed_headers;
  std::map<std::string, std::string> *response_header;
  std::string file_path;
  Userdata() {
    is_chunk = passed_headers = false;
  }
};

/*
  Upon this class we can create an object which we can handle client requests.
*/
class Request
{
private:

  int connected_sock;
  std::map<std::string, std::string> request_header;
  std::map<std::string, std::string> response_header;
  std::string method, url, version;
  std::string file_path;
  std::ofstream filestream;

public:

  Request(int sock);
  ~Request();
  int recieve_request_headers();
  int internet_response_saver();
  void http_https_decider();
  void set_file_path();
  void set_response_attr();
  void create_ip_folder();
  const char * echo_url();
};

#endif
