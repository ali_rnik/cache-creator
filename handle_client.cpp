#include "proxyserver.h"

/*
  This function handling client, with opened socket passed to it. after function
   finished , we must assure that client opened socket will be closed by the
   server.

  @param(void* arg) : opened socket which immedietly free after passing it to
    another function.

  noReturn
*/
void* handle_client(void* arg)
{
  int connected_sock;
  Request* req;

  connected_sock = *(int*) arg;
  free(arg);

  req = new Request(connected_sock);

  req->recieve_request_headers();
  req->http_https_decider();
  req->set_file_path();
  req->create_ip_folder();

  printf("Client with the following URL accepted : %s\n\n", req->echo_url());

  req->internet_response_saver();
  req->set_response_attr();

  printf("Client with following URL Done successfuly: %s\n\n", req->echo_url());

  delete req;
}
