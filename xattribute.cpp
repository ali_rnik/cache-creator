#include "xattribute.h"

Xattribute::Xattribute(string file_path)
{
  m_file_path = file_path;
}

int Xattribute::setx(string name, string value)
{
  int ret, errnum;

  ret = setxattr(m_file_path.c_str() , name.c_str(), value.c_str(), value.size(), 0);
  if(ret == -1)
  {
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  return 0;
}

int Xattribute::getx(string name, string* value)
{
  int value_size, errnum;

  // call function to fetch the value size which is set for name
  value_size = getxattr(m_file_path.c_str(), name.c_str(), NULL, 0);
  if(value_size == -1)
  {
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  char* tmp_value;
  tmp_value = (char*)calloc(value_size, sizeof(tmp_value));

  value_size = getxattr(m_file_path.c_str(), name.c_str(), tmp_value, value_size);
  if(value_size == -1)
  {
    free(tmp_value);
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  *value = tmp_value;

  free(tmp_value);
  return 0;
}

int Xattribute::removex(string name)
{
  int errnum, ret;

  ret = removexattr(m_file_path.c_str(), name.c_str());
  if(ret == -1)
  {
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  return 0;
}

int Xattribute::listx(vector<string>* attr_name)
{
  int list_size, errnum;

  // Call function to get the list attribute size
  list_size = listxattr(m_file_path.c_str(), NULL, 0);
  if(list_size == -1)
  {
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  char *tmp_list, *adr_saver;
  tmp_list = (char*)calloc(list_size, sizeof(tmp_list)+1);

  adr_saver= tmp_list;

  list_size = listxattr(m_file_path.c_str(), tmp_list, list_size);
  if(list_size == -1)
  {
    free(tmp_list);
    errnum = errno;
    fprintf(stderr, "%d: %s: %s", __LINE__, __func__, strerror(errnum));
    return -1;
  }

  string s;

  while (1) {
    s.clear();
    s = tmp_list;
    if(s == "")
      break;

    attr_name->push_back(s);
    tmp_list += strlen(tmp_list)+1;
  }

  free(adr_saver);
  return 0;
}
