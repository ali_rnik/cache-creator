#include "proxyserver.h"

/*
  main function of the program: This function first try to startup the server.
   then it waits forever to accept any client, as soon as it accept the client,
   it will create a thread for it, and handle client request in that thread and
   we are sure that thread must finish whether successfully or on failure, this
   function implemented in a way which will never crash (expected not to crash).

  @param(int argc) : length of argv
  @param(char** argv) : array of strings, which is the paramters of passed main.

  return(success) : 0
  return(failure) : -1
*/
int main(int argc, char** argv)
{
  /* Adding this line cause program to not exit on sig pipe broken */
  signal(SIGPIPE, SIG_IGN);

  pthread_t tid;
  int* connected_sock;
  int sock, port;

  port = atoi(argv[1]);
  if ((sock = server_startup(port)) == -1) {
    ferror(__LINE__, __func__);
    return -1;
  }

  printf("Connected to server on port : %s\n", argv[1]);

  while (1) {
    connected_sock = (int*) malloc(sizeof(connected_sock));
    if ((*connected_sock = accept(sock, NULL, NULL)) == -1) {
      ferror(__LINE__, __func__);
      continue;
    }
    if (pthread_create(&tid, NULL, handle_client, (void*) connected_sock)!=0) {
      close(*connected_sock);
      ferror(__LINE__, __func__);
    }
  }
  return 0;
}
