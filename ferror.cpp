#include "proxyserver.h"

/*
  ferror prints the errors in a pretty way

  @param(int line) : corresponding line number which errors happend there.
  @param(const char* func) : corresponding function which errors happend.

  noReturn
*/
void ferror(int line, const char* func)
{
  int errnum;
  errnum = errno;
  fprintf(stderr, "%d: %s: %s\n", line, func, strerror(errnum));
}
