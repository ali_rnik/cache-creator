#include "proxyserver.h"

/*
  This function is called when internet_response() is called and its duty is
   to send the Curl output to opened socket, also during sending information
   it must identify type of  Transfer-Encoding (chunked, normal) and change
   sending format upon Transfer-Encoding which just fetched.

   @param(void* ptr) : store string of binaries which Curl just fetched.
   @param(size_t sz) : size of each member of ptr.
   @param(size_t nmemb) : number of members of ptr.
   @param(Userdata* ud) : argument which passed from internet_response to this
    function (connected_sock, is_chunk, passed_headers).

   return(success) : number of members which sent to connected_sock.
   return(failure) : LLONG_MAX which is a signal to Curl that error happends.
*/
size_t curl_write(void* ptr, size_t sz, size_t nmemb, Userdata* ud)
{
  std::ofstream filestream;
  size_t ptr_size = nmemb*sz;
  std::string tmps, header, value; // used to fetch and trim information from responses
  char chunk_buf[10] = {0}; // store data block size in chunk format
  int colonpos;

  tmps.clear();

  filestream.open(ud->file_path.data(), std::ios::app | std::ios::out);

  if (strncmp((char*)ptr, CRLF, 2) == 0 && ud->passed_headers == false) {
    ud->passed_headers = true;
    if (write(ud->connected_sock, ptr, ptr_size) == -1) {
      ferror(__LINE__, __func__);
      filestream.close();
      return LLONG_MAX;
    }
    filestream.close();
    return ptr_size;
  }

  if (ud->passed_headers == false) {
    tmps = (char *) ptr;
    colonpos = tmps.find_first_of(":");
    header = tmps.substr(0, colonpos);
    value = tmps.substr(colonpos+2);

    if (tmps.substr(0, 4) != "HTTP") {
      colonpos = tmps.find_first_of(":");
      header = tmps.substr(0, colonpos);
      value = tmps.substr(colonpos+2);
      value.pop_back();
      value.pop_back();

      ud->response_header->operator[](header) = value;
    }
    else {
      tmps.pop_back();
      tmps.pop_back();
      ud->response_header->operator[]("Status-Line") = tmps;
    }

    if (value == "chunked")
      ud->is_chunk = true;
    if (write(ud->connected_sock, ptr, ptr_size) == -1) {
      ferror(__LINE__, __func__);
      filestream.close();
      return LLONG_MAX;
    }
    filestream.close();
    return ptr_size;
  }

  if (ud->is_chunk == true) {
    sprintf(chunk_buf, "%x%s", (unsigned int) ptr_size, CRLF);
    if (write(ud->connected_sock, chunk_buf, strlen(chunk_buf)) == -1) {
      ferror(__LINE__, __func__);
      filestream.close();
      return LLONG_MAX;
    }
    filestream.write(chunk_buf, strlen(chunk_buf));
  }


  if (write(ud->connected_sock, ptr, ptr_size) == -1) {
    ferror(__LINE__, __func__);
    filestream.close();
    return LLONG_MAX;
  }

  filestream.write((const char *) ptr, ptr_size);

  if (ud->is_chunk == true) {
    if (write(ud->connected_sock, CRLF, strlen(CRLF)) == -1) {
      ferror(__LINE__, __func__);
      filestream.close();
      return LLONG_MAX;
    }
    filestream.write(CRLF, strlen(CRLF));
  }

  filestream.close();
  return ptr_size;
}
